from django.urls import path, include, reverse_lazy
from receipts.views import (
    AccountCreateView,
    ReceiptsListView,
    CategoryCreateView,
    ReceiptCreateView,
    CategoryListView,
    AccountListView,
)

urlpatterns = [
    path("", ReceiptsListView.as_view(), name="home"),
    path(
        "accounts/create/",
        AccountCreateView.as_view(),
        name="create_account",
    ),
    path("accounts/", AccountListView.as_view(), name="list_accounts"),
    path(
        "category/create/",
        CategoryCreateView.as_view(),
        name="create_category",
    ),
    path(
        "categories/",
        CategoryListView.as_view(),
        name="list_categories",
    ),
    path("create/", ReceiptCreateView.as_view(), name="create_receipt"),
]
